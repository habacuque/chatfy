import React from 'react'
import { BrowserRouter , Switch , Route } from 'react-router-dom'

import MainPage from 'components/MainPage'
import NotFoundPage from 'components/NotFoundPage'
import SearchPage from 'components/SearchPage'
import ChatPage from 'components/ChatPage'

function App() {
  return (
      <BrowserRouter>
          <Switch>
            <Route exact path="/" component={MainPage} /> 
            <Route exact path="/search" component={SearchPage} /> 
            <Route exact path="/chat/:itemId" component={ChatPage} /> 
            <Route component={NotFoundPage}/>
          </Switch>
      </BrowserRouter>
  )
}

export default App
