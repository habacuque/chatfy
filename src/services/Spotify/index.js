class Spotify {
    constructor() {
        this.client_id='799a90f7c0f944459981205ea4aa4d9d'
        if(process.env.NODE_ENV === 'production' ) { this.redirect_uri=encodeURI('https://chatfy-fdac0.web.app/search') } 
        else { this.redirect_uri=encodeURI('http://localhost:3000/search') }
        this.scope=encodeURI('user-read-currently-playing user-read-playback-state')

        this.authEndPoint = `https://accounts.spotify.com/authorize?client_id=${this.client_id}&response_type=token&redirect_uri=${this.redirect_uri}&scope=${this.scope}`
    }

    async getCurrentPlaying(authToken) {
        const options = { method: 'GET', headers: { Authorization: `Bearer ${authToken}` } }
        const res = await fetch('https://api.spotify.com/v1/me/player/currently-playing',options)

        if(res.status === 204) { throw new Error("You aren't listening to anything on Spotify, or your session is private") }
        else if(res.status === 429) { 
            const timeout = res.headers.get('Retry-After')
            throw new Error(`This app made too much requests to Spotify, everyone will have to try again ${timeout} seconds later`)
        } 
        else if(res.status !== 200) { throw new Error("Couldn't get currently playing track")} 

        const obj = await res.json()
        const song = { id: obj.item.id , name: obj.item.name }
        const album = { id: obj.item.album.id , name: obj.item.album.name }
        const artist = { id: obj.item.album.artists[0].id , name: obj.item.album.artists[0].name }
        return { song , album , artist }
    }

    async getUserName(authToken) {
        const options = { method: 'GET', headers: { Authorization: `Bearer ${authToken}` } }
        const res = await fetch('https://api.spotify.com/v1/me',options)
        if(res.status !== 200) { throw new Error("Couldn't get user name") } 
        const obj = await res.json()
        return obj.display_name
    }
    
}

export default new Spotify()
