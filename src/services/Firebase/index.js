import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const config = {
    apiKey: "AIzaSyCFoxqsgfcIKjN7xlBzmwOaMXHTRUbOa90",
    authDomain: "chatfy-fdac0.firebaseapp.com",
    databaseURL: "https://chatfy-fdac0.firebaseio.com",
    projectId: "chatfy-fdac0",
    storageBucket: "chatfy-fdac0.appspot.com",
    messagingSenderId: "462206717486",
    appId: "1:462206717486:web:f23440a975487dcb2f19bf"
  }


class Firebase {
    constructor() {
        firebase.initializeApp(config)
        this.db = firebase.database()
    }

    async login() {
        const user = firebase.auth().currentUser;
        if(user) { return user.uid }
        const authData = await firebase.auth().signInAnonymously()
        return authData.user.uid
    }
}

export default new Firebase()
