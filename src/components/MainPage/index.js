import React from "react"
import styles from './index.module.css'
import Spotify from 'services/Spotify'

export default function MainPage() {
    return ( 
        <div className={styles.border}>
        <main className={styles.content}>
            <header className={styles.header}>
                <h1 className={styles.title}>Chatfy</h1>
                <h6 className={styles.description}>Chat with people listening to the same things as you</h6>
            </header>
            <a className={styles.button} href={Spotify.authEndPoint}>Spotify Login</a>
        </main>
        </div>
    )
}
