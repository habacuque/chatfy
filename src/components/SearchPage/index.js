import React,{ useState , useEffect } from 'react'
import { Link } from 'react-router-dom'
import Spotify from 'services/Spotify'
import styles from './index.module.css'

function getToken() {
    const url = window.location.href
    if( url.includes("#access_token=") ) {
        return url.split("#access_token=")[1].split('&')[0]
    } 
    return null
}

export default function SearchPage() {

    const [ currentItens, setCurrentItens] = useState("")
    const [ userName , setUserName ] = useState("")
    const [ loading , setLoading ] = useState(true)
    const [ err , setErr ] = useState("")

    useEffect( () => {
        const token = getToken()
        if(!token) { setErr("Couldn't login into Spotify") ; return }
        Spotify.getUserName(token)
          .then( (name) => name )
          .catch( (e) => 'randomssaur' )
          .then( (name) => {
            setUserName(name)
            Spotify.getCurrentPlaying(token).then( (itens) => { setCurrentItens(itens) ; setLoading(false) })
            .catch( (e) => setErr(e.message) )
          })
    },[])



    let content;
    if(err) { content = (
        <>
        <header className={styles.error}>{err}</header> 
        <a href={Spotify.authEndPoint} className={styles.button}>Retry</a>
        </>
    )} else if(loading) { content = <header className={styles.loading}>Loading...</header> }
    else { content =  (
          <>
          <header className={styles.header}>Chat with people listening to the same:</header>
          <nav>
              <Link to={{ pathname:`/chat/${currentItens.song.id}`,state: { userName: userName , itemName: currentItens.song.name} }} className={styles.button}>Song</Link>
              <Link to={{ pathname:`/chat/${currentItens.album.id}`,state: { userName: userName , itemName: currentItens.album.name} }} className={styles.button}>Album</Link>
              <Link to={{ pathname:`/chat/${currentItens.artist.id}`,state: { userName: userName , itemName: currentItens.artist.name} }} className={styles.button}>Artist</Link>
          </nav>
          </>
    )}


    return ( 
        <div className={styles.border}>
        <main className={styles.content}>
            {content}
        </main> 
        </div>
    )
}
