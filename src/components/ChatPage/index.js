import React, { useState , useEffect } from 'react'
import Spotify from 'services/Spotify'
import Service from './Service'
import styles from './index.module.css'

import Message from './Message'

export default function ChatPage(props) {

    const [ err , setErr ] = useState("")
    const [ loading , setLoading ] = useState(true)
    const [ itemId , setItemId ] = useState("")
    const [ itemName , setItemName ] = useState("")
    const [ userName , setUserName ] = useState("")
    const [ messages , setMessages ] = useState([])
    const [ message , setMessage ] = useState([])

    const msgsContainer = React.createRef()
    const handleNewMessage = (msg) => {
        setMessages( messages => [...messages,msg] )
    }

    // scroll when there is new message
    useEffect( () => {
        if(msgsContainer.current) { msgsContainer.current.scrollTop = msgsContainer.current.scrollHeight }
    },[messages])


    // component initialization
    useEffect( () => {
        // case user entered this page by link instead of navigation
        if(!props.location.state) { window.location.href = Spotify.authEndPoint }
        else { 
            const itmId = props.match.params.itemId
            const usrN = props.location.state.userName
            const itmN = props.location.state.itemName
            setItemId(itmId)
            setItemName(itmN)
            Service.setOnline(usrN,itmId).then( (wasAlreadyOnline) => {
                if(wasAlreadyOnline) { setErr("You are already online in this chatroom") }
                else { 
                    setUserName(usrN) 
                    Service.getCurrentOnline(itmId).then( (onlineUsers) => {
                        const onlineMsg = { key: 0 , sender: "system" , content: "online now: "+onlineUsers.toString()}
                        handleNewMessage(onlineMsg)
                        Service.trackMessages(itmId,handleNewMessage)
                        setLoading(false) 
                    })
                }
            }).catch( (e) => { setErr("Couldn't connect to the server") })
            return () => { Service.setOffline(userName,itemId)  }
        }
    },[])

    const handleMsgSubmit = (evt) => {
        evt.preventDefault() //prevent page from reloading
        const msg = message;
        if(message) {
            const msgObj = { sender: userName , content: msg }
            setMessage("")
            Service.sendMsg(msgObj,itemId)
        }
    }

    const handleMsgWrite = (evt) => {
        setMessage(evt.target.value)
    }

    let content
    if(err) { content = (
        <>
        <div className={styles.border}>
            <main className={styles.content}>
                <header className={styles.error}>{err}</header>
                <a href={Spotify.authEndPoint} className={styles.button}>Retry</a>
            </main> 
        </div>
        </>
    )} else if(loading) { content = (
        <>
        <div className={styles.border}>
            <main className={styles.content}>
                <header className={styles.loading}>Loading...</header>
            </main> 
        </div>
        </>
    )} else { content = (
        <>
        <div className={styles.messageContainer}>
            <header className={styles.header}>{itemName}</header>
            <main ref={msgsContainer} className={styles.messages}>
                { messages.map( (m) => <Message key={m.key} msgObj={m} /> ) }
            </main> 
            <form className={styles.messageSend} onSubmit={handleMsgSubmit}>
                <input className={styles.messageSendInput} onChange={handleMsgWrite} type="text" value={message} />
                <button className={styles.messageSendButton} type="submit">Send</button>
            </form>
        </div>
        </>
    )}

    return ( 
        <>
        {content}
        </>
    )
}
