import React from 'react'
import styles from './index.module.css'

export default function ChatPage({msgObj}) {
    return (
        <div className={styles.message}>
            <div className={styles.sender}>{msgObj.sender}</div>
            <div className={styles.content}>{msgObj.content}</div>
        </div>
    )
}
