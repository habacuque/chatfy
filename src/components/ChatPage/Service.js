import Firebase from 'services/Firebase'

class Service {

    async setOnline(name,chatRoom,userRef) {
            // we are using anonymous login , but have to ensure user is logged to access database
            await Firebase.login()
            if(!userRef) { userRef = Firebase.db.ref(`${chatRoom}/online/${name}`) }
            const wasAlreadyOnline = await this._atomicSetOnline(userRef);
            if(wasAlreadyOnline) { return true } 
            return false
    }

    _atomicSetOnline(userRef) {
        return new Promise( (resolve,reject) => {
            userRef.transaction( (user) => {
                // mark user as online and set the tracking
                if(!user) { 
                    userRef.on('value',() => true); 
                    userRef.onDisconnect().remove(); 
                    return true 
                } else { return } // username already taken, abort transaction
            }, (err,commited) => { 
                    if(err) { reject(err) }
                    if(commited) { resolve(false) }
                    else { resolve(true) }
            })
        })
    }

    async getCurrentOnline(chatRoom) {
        await Firebase.login()
        const usersRef = Firebase.db.ref(`${chatRoom}/online`)
        const snapshot = await usersRef.once('value')
        const obj = snapshot.val()
        if(obj) { return Object.keys(obj) }
        return []
    }


    setOffline(name,chatRoom) {
        //const onlineRef = Firebase.db.ref(`online/${chatRoom}`)
        const userRef = Firebase.db.ref(`${chatRoom}/online/${name}`)
        const msgsRef = Firebase.db.ref(`${chatRoom}/messages`)
        // mark user as diconnected
        userRef.off("value")
        userRef.remove() 
        // stop tracking messages
        msgsRef.off("child_added")
    }

    async trackMessages(chatRoom,cb) {
        await Firebase.login()
        const msgsRef = Firebase.db.ref(`${chatRoom}/messages`)
        msgsRef.on('child_added', (data) => {
            const msg = { key: data.key , sender: data.val().sender , content: data.val().content }        
            cb(msg)
        })
    }

    async sendMsg(msgObj,chatRoom) {
        await Firebase.login()
        const msgsRef = Firebase.db.ref(`${chatRoom}/messages`)
        msgsRef.push(msgObj)
    }

}

export default new Service()
